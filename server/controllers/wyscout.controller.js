'use strict';

const path = require('path');

const WyScoutService = require(path.resolve(__basedir, 'services/wyscout.service'));

const getWyScout = async (req, res) => {
  try {
    const result = await WyScoutService.getWyScout(req.query);

    res.status(result.pass ? 200 : 400).json({message: result.message});
  } catch (err) {
    res.status(400).json({message: 'wyscout error while run getWyScout.'});
  }
};

module.exports = {
  getWyScout
};
