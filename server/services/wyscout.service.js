/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable object-curly-newline */
/* eslint-disable no-continue */
/* eslint-disable no-use-before-define */
/* eslint-disable max-len */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-loop-func */
/* eslint-disable no-await-in-loop */

'use strict';

const path = require('path');
const moment = require('moment').utc;
const {OctoOptions} = require('octo-common').utils;

const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const wyscoutDailyEvents = require(path.resolve(__basedir, 'services/wyscout.dailyEvents'));
const wyscoutDailySeasons = require(path.resolve(__basedir, 'services/wyscout.dailySeasons'));
const wyscoutHistorical = require(path.resolve(__basedir, 'services/wyscout.historical'));
const wyscoutManually = require(path.resolve(__basedir, 'services/wyscout.manually'));
const wyscoutAdvanced = require(path.resolve(__basedir, 'services/wyscout.advanced'));
let runSeasons;
const className = 'wyscout.service.';

const getDayFromDate = () => `${moment().year()}${moment().month() + 1}${moment().date()}`;

const runMethod = async (method, isHistorical) => {
  switch (method) {
   case 'dailyEventsScheduleFull':
    await wyscoutDailyEvents.dailyEvents(true);
    await dailySeasons();
    await wyscoutAdvanced.getMatchAdv(true);
    await wyscoutUtils.setLog('Info', 'runMethod dailyEventsScheduleFull', '', `End ${method}`, className, isHistorical);
   break;
   case 'dailyEventsSchedule':
    await wyscoutDailyEvents.dailyEvents(false);
    await wyscoutAdvanced.getMatchAdv(false);
    await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'NewEvents', val: true}], [], 'runMethod');
    await wyscoutUtils.setLog('Info', 'runMethod dailyEventsSchedule', '', `End ${method}`, className, isHistorical);
   break;
   case 'historical': await wyscoutHistorical.historicalEvents(); break;
   case 'historicalFull': await wyscoutHistorical.historicalEventsFull(); break;
   case 'historicalEventsFullFromSql': await wyscoutHistorical.historicalEventsFullFromSql(); break;
   case 'historicalEventsByMatchId': await wyscoutHistorical.historicalEventsByMatchId(); break;
   case 'historicalScoresByMatchId': await wyscoutHistorical.historicalScoresByMatchId(); break;
   case 'getTeamsByMatch': await wyscoutManually.getTeamsByMatch(); break;
   case 'updateEventsTags': await wyscoutManually.updateEventsTags(); break;
   case 'getMatchesDetails': await wyscoutManually.getMatchesDetails(); break;
   case 'getMatchesDetailsArchive': await wyscoutManually.getMatchesDetails(true); break;
   case 'getMatchAdv':
   case 'historicalGetMatchAdv': await wyscoutAdvanced.getMatchAdv(isHistorical); break;
   default: break;
  }
  return Promise.resolve();
 };

 const stopMethod = async (method) => {
   switch (method) {
     case 'daily':
      wyscoutDailyEvents.stopDaily();
      wyscoutDailySeasons.stopDaily();
     break;
     case 'historical': wyscoutHistorical.stopHistorical(); break;
     case 'manually': wyscoutManually.stopManually(); break;
     case 'advanced': wyscoutAdvanced.stopAdvanced(); break;
    default: break;
   }
   return Promise.resolve();
  };

const dailySeasons = async () => {
  let isRun = false;
  if (runSeasons === undefined) {
    runSeasons = getDayFromDate();
    isRun = true;
  } else if (runSeasons !== getDayFromDate()) {
    runSeasons = getDayFromDate();
    isRun = true;
  }
  if (isRun) await wyscoutDailySeasons.dailySeasons();
  return Promise.resolve();
};

const getWyScout = async (options) => {
  const {method} = options || '';
  if (OctoOptions.parseBoolean(options.stopCrawler)) {
    await stopMethod(method);
    return Promise.resolve({message: `End crawl ${method} stopped`});
  }
  if (method === '') {
    await wyscoutUtils.setLog('Info', 'getWyScout', '', 'method is empty');
    return Promise.resolve({message: `End crawl ${method}. Error:method is empty`});
  }
  const isHistorical = method.indexOf('historical') > -1;
  await wyscoutUtils.setLog('Info', 'getWyScout', '', `Start ${method}`, className, isHistorical);
  runMethod(method, isHistorical);
  return Promise.resolve({message: `End crawl ${method}`});
};

module.exports = {
  getWyScout
};
