/* eslint-disable no-continue */
/* eslint-disable max-len */
/* eslint-disable no-await-in-loop */

'use strict';

const path = require('path');
// const moment = require('moment').utc;
const _ = require('lodash');

const Config = require(path.resolve(__basedir, 'config/config'));
const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const className = 'wyscout.manually.';
let isStop = false;

const stopManually = () => {
  isStop = true;
};

const getTeamsByMatch = async () => {
  if (isStop) return Promise.resolve();
  const fn = `${className}getTeamsByMatch`;
  const matches = await wyscoutUtils.postDataProc('[dbo].[GetMatchesForHostorical]', [], [], fn, true);
  let teamsIdsMap = {};
  let teamsMap = {};
  for (let i = 0; i < matches.length; i += 1) {
    const matchId = matches[i].id;
    const teamsByMatch = await wyscoutUtils.getTeamsByMatch(matchId, className, true);
    [teamsIdsMap, teamsMap] = wyscoutUtils.getTeamMaps(matchId, teamsByMatch, teamsIdsMap, teamsMap, true);
    await wyscoutUtils.sleep(100);
  }
  await wyscoutUtils.setTeamsIds(teamsIdsMap);
  await wyscoutUtils.setTeams(teamsMap);
  await wyscoutUtils.setTeamsIds(teamsIdsMap, true);
  return Promise.resolve();
};

const getMatchesDetails = async () => {
  if (isStop) return Promise.resolve();
  const fn = `${className}getMatchesDetails`;
  const matches = await wyscoutUtils.postDataProc('[dbo].[GetMatchesManually]', [], [], fn);
  const matchesList = [];
  let teamsIdsMap = {};
  let teamsMap = {};
  for (let i = 0; i < matches.length; i += 1) {
    const matchId = matches[i].id;

    const teamsByMatch = await wyscoutUtils.getTeamsByMatch(matchId, className);

    let {match} = teamsByMatch;
    // match.date = moment(match.date);
    match.matchId = matchId;
    match.duration = match.duration;
    match.winner = match.winner;
    await wyscoutUtils.sleep(100);
    [teamsIdsMap, teamsMap] = wyscoutUtils.getTeamMaps(matchId, teamsByMatch, teamsIdsMap, teamsMap);
    await wyscoutUtils.sleep(100);
    match = wyscoutUtils.getMatchesScore(match, teamsByMatch.homeTeam, teamsByMatch.awayTeam);
    matchesList.push(match);
  }
  if (matchesList.length > 0) {
    await wyscoutUtils.postDataProc('[dbo].[SetMatchesDate]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matchesList}], `${className}getMatchesDetails`);
  }
  return Promise.resolve();
};


const updateMatchDate = async () => {
  isStop = false;
  const fn = `${className}dailyEvents`;
  const seasonsList = await wyscoutUtils.postDataProc('[dbo].[GetSeasonsActive]', [], [], fn);
  const matchesList = await wyscoutUtils.postDataProc('[dbo].[GetMatchesManually]', [{name: 'isArchive', val: false}, {name: 'lastId', val: 2}], [], fn);
  const matchesMap = await _.mapValues(_.keyBy(matchesList, 'id'), [false]);
  for (let i = 0; i < seasonsList.length; i += 1) {
    if (isStop) return Promise.resolve();
    const seasonId = seasonsList[i].SeasonId;
    const matchesExists = await wyscoutUtils.getMatchesBySeasonApi({seasonId}, fn);
    const matches = [];

    for (let j = 0; j < matchesExists.length; j += 1) {
      const match = matchesExists[j];
      const {matchId} = match;
      if (matchesMap[matchId] === false) {
        matches.push(match);
      }
    }
    if (matches.length > 0) {
      await wyscoutUtils.postDataProc('[dbo].[SetMatchesDate]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], `${className}setMatches`);
    }
  }
  return Promise.resolve();
};


const updateEventsTags = async () => {
  const fn = `${className}updateEventsTags`;
  const matchesList = await wyscoutUtils.postDataProc('[dbo].[GetMatchesManually]', [], [], fn);

  for (let i = 0; i < matchesList.length; i += 1) {
    if (isStop) return Promise.resolve();
    const matchId = matchesList[i].id;
    let eventsTags = [];

    const ret = await wyscoutUtils.getEventsApi(matchId);
    if (ret.length > 0) {
      for (let j = 0; j < ret.length; j += 1) {
        const event = ret[j];
        event.tags = JSON.stringify(event.tags);
        eventsTags = _.concat(eventsTags, wyscoutUtils.parseTags(event.tags, event.matchId, event.id));
      }
      if (eventsTags.length > 0) {
        await wyscoutUtils.postDataProc('[dbo].[SetEventsTags]', [], [{name: 'EventsTagsUD', columns: Config.tableDetails.eventsTags, data: eventsTags}], fn);
      }
    }
  }
  return Promise.resolve();
};
module.exports = {
  getTeamsByMatch,
  stopManually,
  getMatchesDetails,
  updateMatchDate,
  updateEventsTags
};
