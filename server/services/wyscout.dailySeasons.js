/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */

'use strict';

const path = require('path');
const moment = require('moment').utc;
const logger = require('octo-common').services.OctoLogger;

const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const className = 'wyscout.dailySeasons.';
let isStop = false;

const stopDaily = () => { isStop = true; };

const dailySeasons = async () => {
  isStop = false;
  const fnName = 'dailySeasons';
  const fn = `${className}${fnName}`;
  logger.info(`${fn} start`);
  await wyscoutUtils.setLog('Info', fnName, null, 'Start', className);
  const areasList = await wyscoutUtils.getAreasApi(true);
  await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'LastTimeDailyFull', val: moment().toDate()}, {name: 'LastTimeDailySmall', val: null}], [], fn);
  await wyscoutUtils.setLog('Info', fnName, null, 'After getAreas', className);

  if (isStop) {
    await wyscoutUtils.setLog('Info', fnName, null, 'End 1 isStop=true', className);
    return Promise.resolve();
  }

  const competitionsList = await wyscoutUtils.getCompetitionsByAreas(areasList, true);
  await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'LastTimeDailyFull', val: moment().toDate()}, {name: 'LastTimeDailySmall', val: null}], [], fn);
  await wyscoutUtils.setLog('Info', fnName, null, 'After getCompetitions', className);

  if (isStop) {
    await wyscoutUtils.setLog('Info', fnName, null, 'End 2 isStop=true', className);
    return Promise.resolve();
  }

  await wyscoutUtils.getSeasonsByCompetitions(competitionsList, true);
  await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'LastTimeDailyFull', val: moment().toDate()}, {name: 'LastTimeDailySmall', val: null}], [], fn);
  await wyscoutUtils.setLog('Info', fnName, null, 'End', className);
  logger.info(`${fn} end`);
  return Promise.resolve();
};

module.exports = {
  dailySeasons,
  stopDaily
};
