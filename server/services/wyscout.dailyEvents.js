/* eslint-disable max-len */
/* eslint-disable object-curly-newline */
/* eslint-disable object-curly-spacing */
/* eslint-disable no-continue */
/* eslint-disable no-undef */
/* eslint-disable no-await-in-loop */

'use strict';

const path = require('path');
const moment = require('moment').utc;
const _ = require('lodash');

const Config = require(path.resolve(__basedir, 'config/config'));
const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const className = 'wyscout.dailyEvents.';
let isStop = false;

const stopDaily = () => { isStop = true; };

const dailyEvents = async (isFull) => {
  isStop = false;
  const fnName = 'dailyEvents';
  const fn = `${className}${fnName}`;
  const ctrl = await wyscoutUtils.postDataProc('[dbo].[GetCTRL]', [], [], fn);
  if (ctrl.length > 0) {
    const isRunningDailyFull = _.get(ctrl[0], 'isRunningDailyFull') || false;
    const isRunningDailySmall = _.get(ctrl[0], 'isRunningDailySmall') || false;
    if (isFull === true && isRunningDailyFull === true) {
      await wyscoutUtils.setLog('Error', fnName, null, `isFull=${isFull}->Daily Full not running now because prev cron not ended`, className);
      return Promise.resolve();
    }
    if (isFull === false && isRunningDailySmall === true) {
      await wyscoutUtils.setLog('Error', fnName, null, `isFull=${isFull}->Daily Small not running now because prev cron not ended`, className);
      return Promise.resolve();
    }
  }
  await wyscoutUtils.setLog('Info', fnName, null, `Start isFull=${isFull}`, className);
  const seasonsList = await wyscoutUtils.postDataProc('[dbo].[GetSeasonsActive]', [{name: 'isFull', val: isFull}], [], fn);
  let isNewEvents = false;
  for (let i = 0; i < seasonsList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, `End 1 isFull=${isFull}->isStop=true`, className);
      return Promise.resolve();
    }
    await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'LastTimeDailyFull', val: isFull ? moment().toDate() : null}, {name: 'LastTimeDailySmall', val: isFull ? null : moment().toDate() }], [], fn);
    const seasonId = seasonsList[i].SeasonId;

    const matchesExistDb = await wyscoutUtils.getMatchesBySeason(seasonId, fn, isFull);
    const matchesApi = await wyscoutUtils.getMatchesBySeasonApi({seasonId}, isFull);
    let matches = [];
    let teamsIdsMap = {};
    let teamsMap = {};
    await wyscoutUtils.setLog('Info', fnName, null, `Start isFull=${isFull}->i:${i}->seasonId:${seasonId}->matchesApi:${matchesApi.length}`, className);
    for (let j = 0; j < matchesApi.length; j += 1) {
      if (isStop) {
        await wyscoutUtils.setLog('Info', fnName, null, `End 2 isFull=${isFull}->isStop=true`, className);
        return Promise.resolve();
      }
      await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'LastTimeDailyFull', val: isFull ? moment().toDate() : null}, {name: 'LastTimeDailySmall', val: isFull ? null : moment().toDate()}], [], fn);
      const match = matchesApi[j];
      const {matchId} = match;
      if (matchId === 0) continue;
      if (matchesExistDb[matchId] === false) continue;
      let countEvents = 0;
      [matches, teamsIdsMap, teamsMap, countEvents] = await wyscoutUtils.getTeamsMatchEventsBySeason(matchId, match, teamsIdsMap, teamsMap, matches, fn, isFull);
      if (isNewEvents === false && countEvents > 0) isNewEvents = true;
      await wyscoutUtils.setLog('Info', fnName, null, `->i:${i}->j:${j}>isFull=${isFull}-->matchId:${matchId}->countEvents=${countEvents}`, className);
    }
    if (matches.length > 0) {
      await wyscoutUtils.postDataProc('[dbo].[SetMatches]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn);
      await wyscoutUtils.setTeamsIds(teamsIdsMap, fn);
      await wyscoutUtils.setTeams(teamsMap, fn);
    }
    await wyscoutUtils.setLog('Info', fnName, null, `End isFull=${isFull}->seasonId:${seasonId}`, className);
  }
  await wyscoutUtils.setLog('Info', fnName, null, `isFull=${isFull}->isNewEvents=${isNewEvents}`, className);

  // if (isNewEvents === true) {
  //   await wyscoutUtils.postDataProc('[dbo].[SetCTRL]', [{name: 'NewEvents', val: true}], [], fn);
  // }

  await wyscoutUtils.setLog('Info', fnName, null, `isFull=${isFull}->Before SendMail`, className);
  await wyscoutUtils.postDataProc('[dbo].[SendMail]', [{name: 'isFull', val: isFull}], [], fn);
  await wyscoutUtils.setLog('Info', fnName, null, `End isFull=${isFull}`, className);

  return Promise.resolve();
};

module.exports = {
  dailyEvents,
  stopDaily
};
