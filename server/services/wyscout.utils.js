/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-continue */
/* eslint-disable no-use-before-define */
/* eslint-disable no-await-in-loop */
/* eslint-disable object-curly-spacing */
/* eslint-disable object-curly-newline */
/* eslint-disable no-param-reassign */

'use strict';

// #region References
const axios = require('axios');
const path = require('path');
const util = require('util');
const _ = require('lodash');
const logger = require('octo-common').services.OctoLogger;
const sql = require('octo-common').services.OctoSqlService;

const Config = require(path.resolve(__basedir, 'config/config'));
// #endregion

// #region  Local Variables
const className = 'wyscout.utils.';
// #endregion

// #region Common
const sleep = async milliseconds => new Promise(resolve => setTimeout(resolve, milliseconds));

const convertObjectToArray = (obj) => {
  let arr = [];
  obj = JSON.parse(obj);
  if (!Array.isArray(obj)) arr.push(obj);
  else arr = _.merge(arr, obj);
  return arr;
};

const parseTags = (tags, matchId, eventId, isHistorical) => {
  const returnArray = [];
  try {
    if (tags) {
      const tagsArr = convertObjectToArray(tags) || [];
      for (let i = 0; i < tagsArr.length; i += 1) {
        returnArray[i] = {
          matchId,
          eventId,
          tagId: tagsArr[i].id
        };
      }
    }
  } catch (error) {
    setLog('Error', 'parseTags', error, `tags: ${JSON.stringify(tags)}`, className, isHistorical);
  }
  return returnArray;
};

const parsePositions = (positions) => {
  if (positions) {
    const pos = convertObjectToArray(positions) || [];
    const returnObject = {
      startX: pos.length > 0 ? pos[0].x : null,
      startY: pos.length > 0 ? pos[0].y : null,
      endX: pos.length > 1 ? pos[1].x : null,
      endY: pos.length > 1 ? pos[1].y : null
    };
    return returnObject;
  }
  return {startX: null, startY: null, endX: null, endY: null};
};

const getTeamMaps = (matchId, teamsByMatch, teamsIdsMap, teamsMap, isHistorical) => {
  const tId = {...teamsIdsMap};
  const tm = {...teamsMap};
  try {
    // if (teamsByMatch.homeTeamId !== -1 || teamsByMatch.awayTeamId !== -1) {
    tId[matchId] = {matchId, homeTeamId: teamsByMatch.homeTeamId, awayTeamId: teamsByMatch.awayTeamId};
    // }
    if (teamsByMatch.homeTeam || teamsByMatch.awayTeam) {
      if (teamsByMatch.homeTeamId !== -1) tm[teamsByMatch.homeTeamId] = teamsByMatch.homeTeam;
      if (teamsByMatch.awayTeamId !== -1) tm[teamsByMatch.awayTeamId] = teamsByMatch.awayTeam;
    }
  } catch (error) {
    setLog('Error', 'getTeamMaps', error, `matchId: ${matchId}`, className, isHistorical);
  }

  return [tId, tm];
};

const getResponseFromSql = response => _.get(response, 'recordset') || [];

const setLog = async (logType, fn, error, desc, classNameCall, isHistorical) => {
  const err = _.get(error, 'message') ? `->error message: ${_.get(error, 'message')}` : '';
  const errFull = util.inspect(error) ? `->error inspect message: ${util.inspect(error)}` : '';
  try {
    const sqlProc = {
      proc: '[dbo].[SetLogs]',
      params: [{name: 'logType', val: logType}, {name: 'fn', val: fn}, {name: 'error', val: err}, {name: 'desc', val: desc}, {name: 'crawlerName', val: classNameCall}],
      tables: []
    };
    await sql.runSqlProc(Config.configSqlWyScout, sqlProc);
    // await sql.runSqlProc(isHistorical ? configWyscoutHistorical : configWyscoutProd, sqlProc);
  } catch (er) {
    const erFull = util.inspect(er) ? `->error inspect message: ${util.inspect(er)}` : '';
    logger.error(`setLog->Error->${fn}->isHistorical:${isHistorical}->catch ${err}->catch 2:${erFull}`);
  }
};

const postData = async (sqlQuery, isReturn, fn, isHistorical) => {
  try {
    const responseData = await sql.runSqlQuery(Config.configSqlWyScout, sqlQuery);
    // const responseData = await sql.runSqlQuery(isHistorical ? configWyscoutHistorical : configWyscoutProd, sqlQuery);
    if (isReturn) {
      const json = JSON.parse(responseData.recordset[0].json) || [];
      await Promise.all(json);
      return json;
    }
  } catch (error) {
    await setLog('Error', 'postData', error, `sqlQuery: ${sqlQuery}`, `${className}${fn}`, isHistorical);
  }
  return null;
};

const postDataProc = async (proc, params, tables, fn, isHistorical, timeout) => {
  const sqlProc = {proc, params, tables};
  try {
    const response = await sql.runSqlProc(Config.configSqlWyScout, sqlProc, timeout);
    // const response = await sql.runSqlProc(isHistorical ? configWyscoutHistorical : configWyscoutProd, sqlProc, timeout);
    return getResponseFromSql(response);
  } catch (error) {
    await setLog('Error', 'postDataProc', error, '', `${className}${fn}`, isHistorical);
  }
  return null;
};

const getData = async (method, isHistorical) => {
  try {
    const responseData = await axios.get(`${Config.API_URL}${method}`, {headers: {Authorization: isHistorical ? Config.tokenHistorical : Config.tokenDaily}});
    return responseData.data;
  } catch (error) {
    await setLog('Error', 'getData', error, `url: ${Config.API_URL}${method}`, className, isHistorical);
  }
  return null;
};
// #endregion

// #region Api
const getAreasApi = async (isHistorical) => {
  const areas = await getData('areas', isHistorical);
  const areasList = _.get(areas, 'areas') || [];
  if (areasList.length > 0) await postDataProc('[dbo].[SetAreas]', [], [{name: 'AreasUD', columns: Config.tableDetails.areas, data: areasList}], `${className}getAreasApi`);
  return Promise.resolve(areasList);
};

const getCompetitonsApi = async (areasList, isHistorical) => {
  const alpha3code = _.get(areasList, 'alpha3code');
  if (!alpha3code || alpha3code.trim() === '') return [];
  const responseData = await getData(`competitions?areaId=${alpha3code}`, isHistorical);
  return _.get(responseData, 'competitions') || [];
};

const getTeamApi = async (matchId, isHistorical) => {
  const response = await getData(`matches/${matchId}?details=teams`, isHistorical);
  return response;
};

const getMatchesBySeasonApi = async (season, isHistorical) => {
  const seasonId = _.get(season, 'seasonId');
  if (!seasonId) return [];
  const response = await getData(`seasons/${seasonId}/matches`, isHistorical);
  return _.get(response, 'matches') || [];
};

const getEventsApi = async (id, isHistorical) => {
  const response = await getData(`matches/${id}/events`, isHistorical);
  return _.get(response, 'events') || [];
};

const getMatchAdvApi = async (id, isHistorical) => {
  const response = await getData(`matches/${id}/advancedstats`, isHistorical);
  return response || {};
};
// #endregion

const getCompetitionsByAreas = async (areasList, isHistorical) => {
  const areasMap = areasList.reduce((acc, cur) => { acc[cur.alpha3code] = cur.name; return acc; }, {});
  let competitionsList = [];
  for (let i = 0; i < (areasList.length) + 1; i += 1) {
    try {
      const area = areasList[i];
      await sleep(200);
      const ret = await getCompetitonsApi(area, isHistorical);
      const league = ret.map(item => ({
        id: _.get(item, 'wyId'),
        name: `${areasMap[_.get(item, 'area.alpha3code')]} ${_.get(item, 'name')}`,
        area: _.get(item, 'area.alpha3code'),
        format: _.get(item, 'format'),
        type: _.get(item, 'type'),
        category: _.get(item, 'category'),
        gender: _.get(item, 'gender'),
        divisionLevel: _.get(item, 'divisionLevel'),
        OriginalName: _.get(item, 'name')
      }));
      competitionsList = competitionsList.concat(league);
    } catch (error) {
      await setLog('Error', 'getCompetitionsByAreas', error, '', className, isHistorical);
    }
  }
  await postDataProc('[dbo].[SetLeagues]', [], [{name: 'LeaguesUD', columns: Config.tableDetails.leagues, data: competitionsList}], `${className}getCompetitionsByAreas`);
  return Promise.resolve(competitionsList);
};

const getSeasonsByCompetitions = async (competitionsList, isHistorical) => {
  let seasonArr = [];
  try {
    await setLog('Info', 'getSeasonsByCompetitions', '', `Start competitionsList.length:${competitionsList.length}`, className, isHistorical);
    for (let i = 0; i < competitionsList.length; i += 1) {
      const competitionId = _.get(competitionsList[i], 'id') || 0;
      if (competitionId === 0) continue;
      await setLog('Info', 'getSeasonsByCompetitions', '', `competitionId:${competitionId}`, className, isHistorical);
      try {
        await sleep(200);
        const response = await getData(`competitions/${competitionId}/seasons`, isHistorical);
        const seasons = _.get(response, 'seasons') || [];
        seasonArr = seasonArr.concat(seasons);
      } catch (error) {
        await setLog('Error', 'getSeasonsByCompetitions 1', error, `competitionId:${competitionId}`, className, isHistorical);
      }
    }
    await setSeasons(seasonArr, isHistorical);
  } catch (error) {
    await setLog('Error', 'catch getSeasonsByCompetitions 2', error, '', className, isHistorical);
  }
  await setLog('Info', 'getSeasonsByCompetitions', '', 'End', className, isHistorical);
  return Promise.resolve(seasonArr);
};
const getMatchesBySeason = async (seasonId, fn, isHistorical) => {
  const matchesExists = await postDataProc('GetMatchesBySeason', [{name: 'seasonId', val: seasonId}], [], `${fn}.getMatchesBySeason`, isHistorical);
  const matchesMap = await _.mapValues(_.keyBy(matchesExists, 'id'), [false]);
  return Promise.resolve(matchesMap || {});
};

const getMatchScores = oneTeam => ({score: _.get(oneTeam, 'score'), scoreHT: _.get(oneTeam, 'scoreHT'), scoreET: _.get(oneTeam, 'scoreET'), scoreP: _.get(oneTeam, 'scoreP')});

const getMatchesScore = (match, homeTeam, awayTeam) => {
  const m = {...match};
  m.h_score = _.get(homeTeam, 'scores.score');
  m.h_scoreHT = _.get(homeTeam, 'scores.scoreHT');
  m.h_scoreET = _.get(homeTeam, 'scores.scoreET');
  m.h_scoreP = _.get(homeTeam, 'scores.scoreP');
  m.a_score = _.get(awayTeam, 'scores.score');
  m.a_scoreHT = _.get(awayTeam, 'scores.scoreHT');
  m.a_scoreET = _.get(awayTeam, 'scores.scoreET');
  m.a_scoreP = _.get(awayTeam, 'scores.scoreP');
  return m;
};

const getTeamsByMatch = async (matchId, fn, isHistorical) => {
  let homeTeamId = -1;
  let awayTeamId = -1;
  let homeTeam;
  let awayTeam;
  let winner;
  let duration;
  let match;
  try {
    match = await getTeamApi(matchId, isHistorical);
    if (match) {
      winner = _.get(match, 'winner');
      duration = _.get(match, 'duration');
      const {teamsData} = match;
      if (teamsData) {
        Object.keys(teamsData).forEach((key) => {
          const oneTeam = teamsData[key] || {};
          if (oneTeam.side === 'home') {
            homeTeamId = oneTeam.teamId;
            homeTeam = oneTeam.team || {};
            homeTeam.scores = getMatchScores(oneTeam);
          } else if (oneTeam.side === 'away') {
            awayTeamId = oneTeam.teamId;
            awayTeam = oneTeam.team || {};
            awayTeam.scores = getMatchScores(oneTeam);
          }
        });
      }
    }
  } catch (error) {
    await setLog('Error', 'getTeamsByMatch', error, `match: ${matchId}`, fn, isHistorical);
  }
  return Promise.resolve({ homeTeamId, awayTeamId, homeTeam, awayTeam, winner, duration, match });
};

const getEvents = async (id, fn, isHistorical) => {
  let countEvents = 0;
  fn = `${fn}getEvents.`;
  try {
    let eventsTags = [];
    const ret = await getEventsApi(id, isHistorical);
    countEvents = ret.length;
    if (countEvents > 0) {
      for (let i = 0; i < ret.length; i += 1) {
        const event = ret[i];
        event.positions = JSON.stringify(event.positions);
        event.tags = JSON.stringify(event.tags);
        const positions = parsePositions(event.positions);
        event.startX = positions.startX;
        event.startY = positions.startY;
        event.endX = positions.endX;
        event.endY = positions.endY;
        eventsTags = _.concat(eventsTags, parseTags(event.tags, event.matchId, event.id, isHistorical));
      }

      await postDataProc('[dbo].[SetEvents]', [{name: 'matchId', val: id}], [{name: 'EventsUD', columns: Config.tableDetails.events, data: ret}], fn, isHistorical);

      if (eventsTags.length > 0) {
        await postDataProc('[dbo].[SetEventsTags]', [], [{name: 'EventsTagsUD', columns: Config.tableDetails.eventsTags, data: eventsTags}], fn, isHistorical);
      }
    } else {
      await setLog('Info', 'getEvents', null, `matchId:${id} events not exists`, className, isHistorical);
    }
  } catch (error) {
    await setLog('Error', 'catch getEvents', error, `matchId:${id}`, className, isHistorical);
  }
  return Promise.resolve(countEvents);
};

const getTeamsMatchEventsBySeason = async (matchId, match, teamsIdsMap, teamsMap, matches, fn, isHistorical) => {
  let m = {...match};
  let tId = {...teamsIdsMap};
  let tm = {...teamsMap};
  const ms = [...matches];
  let countEvents = 0;
  await sleep(100);
  try {
    const teamsByMatch = await getTeamsByMatch(matchId, `${className}${fn}.getTeamsMatchEventsBySeason`, isHistorical);
    m.duration = teamsByMatch.duration;
    m.winner = teamsByMatch.winner;

    [tId, tm] = getTeamMaps(matchId, teamsByMatch, tId, tm, isHistorical);

    m = getMatchesScore(m, teamsByMatch.homeTeam, teamsByMatch.awayTeam);

    await sleep(100);
    countEvents = await getEvents(matchId, `${className}${fn}.getTeamsMatchEventsBySeason`, isHistorical);
    m.hasEvents = countEvents > 0;
    ms.push(m);
  } catch (error) {
    await setLog('Error', 'catch getTeamsMatchEventsBySeason', error, '', className, isHistorical);
  }
  return [ms, tId, tm, countEvents];
};

const setTeams = async (teamsMap, fn) => {
  const teams = Object.keys(teamsMap).map(key => teamsMap[key]);
  if (teams.length > 0) {
    const teamSql = teams.map(item => ({
      id: _.get(item, 'wyId'),
      name: _.get(item, 'name'),
      officialName: _.get(item, 'officialName'),
      city: _.get(item, 'area.city'),
      area: _.get(item, 'area.alpha3code'),
      type: _.get(item, 'type'),
      category: _.get(item, 'category'),
      gender: _.get(item, 'gender')
    }));
    await postDataProc('[dbo].[SetTeams]', [], [{name: 'TeamsUD', columns: Config.tableDetails.teams, data: teamSql}], `${fn}setTeams`);
  }
  return Promise.resolve();
};

const setTeamsIds = async (teamsIdsMap, fn, isHistorical) => {
  const teamsIds = Object.keys(teamsIdsMap).map(key => teamsIdsMap[key]);
  if (teamsIds.length > 0) {
    await postDataProc('[dbo].[SetTeamsIds]', [], [{name: 'TeamsIdsUD', columns: Config.tableDetails.teamsIds, data: teamsIds}], `${fn}setTeamsIds`, isHistorical);
  }
  return Promise.resolve();
};

const setSeasons = async (seasonsFull, isHistorical) => {
  let setSeasonsLoop = 0;
  let seasonsList = [];
  try {
    for (let i = 0; i < seasonsFull.length; i += 1) {
      const season = await getData(`seasons/${seasonsFull[i].seasonId}`, isHistorical);
      if (!season || !_.get(season, 'wyId')) continue;
      seasonsList = seasonsList.concat(season);
      if (seasonsList.length > 2000) {
        await postDataProc('[dbo].[SetSeasons]', [], [{name: 'SeasonsUD', columns: Config.tableDetails.seasons, data: seasonsList}], `${className}setSeasons`);
        setSeasonsLoop += 1;
        await setLog('Info', 'setSeasons', null, `setSeasonsLoop=${setSeasonsLoop}`, className, isHistorical);
        seasonsList = [];
      }
    }
    if (seasonsList.length > 0) {
      await postDataProc('[dbo].[SetSeasons]', [], [{name: 'SeasonsUD', columns: Config.tableDetails.seasons, data: seasonsList}], `${className}setSeasons`);
    }
  } catch (error) {
    await setLog('Error', 'catch setSeasons', error, '', className, isHistorical);
  }

  return Promise.resolve(seasonsList);
};

module.exports = {
  setLog,
  postData,
  postDataProc,
  getData,
  sleep,
  getAreasApi,
  getTeamApi,
  getMatchesBySeasonApi,
  getCompetitionsByAreas,
  getSeasonsByCompetitions,
  getTeamsByMatch,
  setTeamsIds,
  setTeams,
  getMatchesScore,
  getTeamMaps,
  getMatchesBySeason,
  getTeamsMatchEventsBySeason,
  parseTags,
  getEventsApi,
  getMatchScores,
  getEvents,
  getMatchAdvApi
};
