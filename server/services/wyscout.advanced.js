/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */

'use strict';

// #region References
const path = require('path');
const _ = require('lodash');

// #endregion

// #region  Local Variables

const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const Config = require(path.resolve(__basedir, 'config/config'));
const className = 'wyscout.advanced.';
let isStop = false;

// #endregion

// #region Common
const stopAdvanced = () => { isStop = true; };
// #endregion

// #region Advanced Stats

const getTeamSide = (match, teamId) => {
  const homeTeamId = _.get(match, 'homeTeamId') || '';
  const awayTeamId = _.get(match, 'awayTeamId') || '';
  if (homeTeamId.toString() === teamId) return 'h_';
  if (awayTeamId.toString() === teamId) return 'a_';
  return null;
};

const getFieldName = (suff, firstKey, thirdKeys) => {
  let field = _.filter(Config.tableDetails.matchAdvanced, x => x.name === `${suff}${firstKey}_${thirdKeys}`);
  if (field.length > 0) return field[0].name;
  field = _.filter(Config.tableDetails.matchAdvanced, x => x.name === `${suff}${thirdKeys}`);
  if (field.length > 0) return field[0].name;
  return null;
};

const validateMatchInfo = async (match, matchId, isHistorical) => {
  let ifContinue = false;
  if (Object.keys(match).length > 0) {
    const checkTeam = _.get(match, 'teams') || [];
    if (Object.keys(checkTeam).length > 0 && _.get(checkTeam[Object.keys(checkTeam)[0]], 'teamId') > 0) ifContinue = true;
  }

  if (!ifContinue) {
    await wyscoutUtils.setLog('Info', 'validateMatchInfo', null, `match info is empty->matchId:${matchId}`, className, isHistorical);
  }
  return Promise.resolve(ifContinue);
};
const getMatchAdv = async (isFull) => {
  isStop = false;
  const fnName = 'getMatchAdv';
  const fn = `${className}${fnName}`;
  let matchesAdv = [];
  await wyscoutUtils.setLog('Info', fnName, null, 'Start', className, isFull);
  const matchesList = await wyscoutUtils.postDataProc('[dbo].[GetMatchesAdv]', [{name: 'isFull', val: isFull}], [], fn, isFull);
  try {
    for (let i = 0; i < matchesList.length; i += 1) {
      if (isStop) {
        await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, isFull);
        return Promise.resolve();
      }
      const matchId = matchesList[i].id;
      const match = await wyscoutUtils.getMatchAdvApi(matchId, isFull);
      const ifContinue = await validateMatchInfo(match, matchId, isFull);
      if (!ifContinue) continue;
      const adv = {};
      const firstKeys = Object.keys(match);
      for (let j = 0; j < firstKeys.length; j += 1) {
        const item = match[firstKeys[j]]; // general,prossesion ...

        const secondKeys = Object.keys(item); // homeId,awayId or totalTime, deadTime
        if (secondKeys.length === 0) continue;
        for (let k = 0; k < secondKeys.length; k += 1) {
          const team = item[secondKeys[k]];
          if (typeof team === 'object') { // one team
            const suff = getTeamSide(matchesList[i], secondKeys[k]);
            if (suff === null) continue;
            const thirdKeys = Object.keys(team);
            for (let x = 0; x < thirdKeys.length; x += 1) {
              const fieldName = getFieldName(suff, firstKeys[j], thirdKeys[x]);
              if (fieldName === null) continue;
              adv[fieldName] = team[thirdKeys[x]];
            }
          } else adv[secondKeys[k]] = team;
        }
      }
      if (Object.keys(adv).length > 0) {
        adv.matchId = matchId;
        adv.homeTeamId = matchesList[i].homeTeamId;
        adv.awayTeamId = matchesList[i].awayTeamId;
        matchesAdv.push(adv);
      }

      if (matchesAdv.length >= 1000) {
        await wyscoutUtils.postDataProc('[dbo].[SetMatchesAdv]', [], [{name: 'MatchesAdvUD', columns: Config.tableDetails.matchAdvanced, data: matchesAdv}], fn, isFull);
        matchesAdv = [];
      }
    }
    if (matchesAdv.length > 0) {
      await wyscoutUtils.postDataProc('[dbo].[SetMatchesAdv]', [], [{name: 'MatchesAdvUD', columns: Config.tableDetails.matchAdvanced, data: matchesAdv}], fn, isFull);
    }
  } catch (error) {
    await wyscoutUtils.setLog('Error', fnName, error, '', className, isFull);
  }
  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, isFull);
  return Promise.resolve();
};

// #endregion

module.exports = {
  getMatchAdv,
  stopAdvanced
};
