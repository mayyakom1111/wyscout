/* eslint-disable no-loop-func */
/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable object-curly-newline */
/* eslint-disable object-curly-spacing */
/* eslint-disable no-continue */
/* eslint-disable no-undef */
/* eslint-disable no-await-in-loop */

'use strict';

const path = require('path');
const _ = require('lodash');
const moment = require('moment').utc;
// const logger = require('octo-common').services.OctoLogger;

const wyscoutUtils = require(path.resolve(__basedir, 'services/wyscout.utils'));
const Config = require(path.resolve(__basedir, 'config/config'));
const className = 'wyscout.historical.';
let isStop = false;

const stopHistorical = () => { isStop = true; };


const getEventsBySeason = async (fn, seasonId, i) => {
  const fnName = 'getEventsBySeason';
  fn = `${fn}${fnName}`;
  // logger.info(`${fn} i:${i}->seasonId:${seasonId}`);
  const matchesExistDb = await wyscoutUtils.getMatchesBySeason(seasonId, fn, true);
  const matchesApi = await wyscoutUtils.getMatchesBySeasonApi({seasonId}, fn, true);
  let matches = [];
  let teamsIdsMap = {};
  let teamsMap = {};
  await wyscoutUtils.setLog('Info', fnName, null, `Start i:${i}->seasonId:${seasonId}->matchesApi:${matchesApi.length}`, fn, true);
  for (let j = 0; j < matchesApi.length; j += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, true);
      return Promise.resolve();
    }
    const match = matchesApi[j];
    const {matchId} = match;
    await wyscoutUtils.setLog('Info', fnName, null, `matchId:${matchId}->j=${j}`, className, true);
    if (matchId === 0) {
      await wyscoutUtils.setLog('Info', fnName, null, `matchId=0->j=${j}`, className, true);
      continue;
    }
    if (matchesExistDb[matchId] === false) {
      await wyscoutUtils.setLog('Info', fnName, null, `matchId:${matchId} Events exists in DB->j=${j}`, className, true);
      continue;
    }
    if (!moment(match.date).isAfter('2013-01-01', 'year')) {
      await wyscoutUtils.setLog('Info', fnName, null, `matchId:${matchId}->data ${match.date} after 2013-01-01->j=${j}`, className, true);
      continue;
    }
    let countEvents = 0;
    [matches, teamsIdsMap, teamsMap, countEvents] = await wyscoutUtils.getTeamsMatchEventsBySeason(matchId, match, teamsIdsMap, teamsMap, matches, fn, true);
    await wyscoutUtils.setLog('Info', fnName, null, `matchId:${matchId}->countEvents=${countEvents}`, className, true);
  }
  if (matches.length > 0) {
    await wyscoutUtils.postDataProc('[dbo].[SetMatches]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn, true);
    await wyscoutUtils.setTeamsIds(teamsIdsMap, fn);
    await wyscoutUtils.setTeams(teamsMap, fn);
  }
  await wyscoutUtils.postDataProc('[dbo].[SetHistorical]', [{name: 'seasonId', val: seasonId}], [], fn, true);

  await wyscoutUtils.setLog('Info', fnName, null, `End seasonId:${seasonId}`, fn, true);

  return Promise.resolve();
};

const historicalEventsFullFromSql = async () => {
  isStop = false;
  const fnName = 'historicalEventsFullFromSql';
  const fn = `${className}${fnName}`;
  // logger.info(`${fn} start`);

  const seasonsList = await wyscoutUtils.postDataProc('[dbo].[GetSeasonsForHistorical]', [{name: 'hasHistory', val: false}], [], fn, true);
  for (let i = 0; i < seasonsList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, true);
      return Promise.resolve();
    }
    await getEventsBySeason(fn, seasonsList[i].SeasonId, i);
  }

  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, true);
  // logger.info(`${fn} end`);
  return Promise.resolve();
};

const historicalEventsFull = async () => {
  isStop = false;
  const fnName = 'historicalEventsFull';
  const fn = `${className}${fnName}`;
  // logger.info(`${fn} start`);

  const areasList = await wyscoutUtils.getAreasApi();
  await wyscoutUtils.setLog('Info', fnName, null, 'After get Areas', className, true);

  if (isStop) {
    await wyscoutUtils.setLog('Info', fnName, null, 'End 1 isStop=true', className, true);
    return Promise.resolve();
  }

  const competitionsList = await wyscoutUtils.getCompetitionsByAreas(areasList, true);
  await wyscoutUtils.setLog('Info', fnName, null, 'After get Competitions', className, true);

  if (isStop) {
    await wyscoutUtils.setLog('Info', fnName, null, 'End 2 isStop=true', className, true);
    return Promise.resolve();
  }

  const seasonsList = await wyscoutUtils.getSeasonsByCompetitions(competitionsList, true);
  await wyscoutUtils.setLog('Info', fnName, null, 'After get Seasons', className, true);

  const seasonsHasHistory = await wyscoutUtils.postDataProc('[dbo].[GetSeasonsForHistorical]', [], [], fn, true);
  const seasonsHasHistoryMap = await _.mapValues(_.keyBy(seasonsHasHistory, 'SeasonId'), [false]) || {};
  for (let i = 0; i < seasonsList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End 3 isStop=true', className, true);
      return Promise.resolve();
    }
    const {seasonId} = seasonsList[i];
    if (seasonsHasHistoryMap[seasonId] === false) continue;
    await getEventsBySeason(fn, seasonId, i);
  }

  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, true);
  // logger.info(`${fn} end`);
  return Promise.resolve();
};

const historicalEvents = async () => {
  isStop = false;
  const fnName = 'historicalEvents';
  const fn = `${className}${fnName}`;
  // logger.info(`${fn} start`);
  await wyscoutUtils.setLog('Info', fnName, null, 'Start', className, true);
  const seasonsList = await wyscoutUtils.postDataProc('[dbo].[GetSeasonsForHistorical]', [], [], fn, true);
  for (let i = 0; i < seasonsList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, true);
      return Promise.resolve();
    }
    const {seasonId} = seasonsList[i];
    await getEventsBySeason(fn, seasonId, i);
  }
  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, true);
  // logger.info(`${fn} end`);
  return Promise.resolve();
};

const historicalEventsByMatchId = async () => {
  isStop = false;
  const fnName = 'historicalEventsByMatchId';
  const fn = `${className}${fnName}`;
  // logger.info(`${fn} start`);
  await wyscoutUtils.setLog('Info', fnName, null, 'Start', className, true);
  const matchesList = await wyscoutUtils.postDataProc('[dbo].[GetMatchesForHostorical]', [], [], fn, true);
  let teamsIdsMap = {};
  let teamsMap = {};
  let matches = [];
  for (let i = 0; i < matchesList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, true);
      return Promise.resolve();
    }
    const matchId = matchesList[i].id;
    await wyscoutUtils.setLog('Info', fnName, null, `i:${i}-->matchId:${matchId}`, fn, true);

    await wyscoutUtils.sleep(100);

    const match = await wyscoutUtils.getTeamApi(matchId, true);
    if (match) {
      let m = {};
      m.matchId = _.get(match, 'wyId');
      m.label = _.get(match, 'label');
      m.date = _.get(match, 'date');
      m.dateutc = _.get(match, 'dateutc');
      m.status = _.get(match, 'status');
      m.competitionId = _.get(match, 'competitionId');
      m.seasonId = _.get(match, 'seasonId');
      m.roundId = _.get(match, 'roundId');
      m.gameweek = _.get(match, 'gameweek');
      m.duration = _.get(match, 'duration');
      m.winner = _.get(match, 'winner');

      let homeTeamId = -1;
      let awayTeamId = -1;
      let homeTeam;
      let awayTeam;
      const {teamsData} = match;
      if (teamsData) {
        Object.keys(teamsData).forEach((key) => {
          const oneTeam = teamsData[key] || {};
          if (oneTeam.side === 'home') {
            homeTeamId = oneTeam.teamId;
            homeTeam = oneTeam.team || {};
            homeTeam.scores = wyscoutUtils.getMatchScores(oneTeam);
          } else if (oneTeam.side === 'away') {
            awayTeamId = oneTeam.teamId;
            awayTeam = oneTeam.team || {};
            awayTeam.scores = wyscoutUtils.getMatchScores(oneTeam);
          }
        });
      }
      const {winner, duration} = m;
      const teamsByMatch = { homeTeamId, awayTeamId, homeTeam, awayTeam, winner, duration, match };
      [teamsIdsMap, teamsMap] = wyscoutUtils.getTeamMaps(matchId, teamsByMatch, teamsIdsMap, teamsMap, true);
      m = wyscoutUtils.getMatchesScore(m, teamsByMatch.homeTeam, teamsByMatch.awayTeam);

      await wyscoutUtils.sleep(100);

      const countEvents = await wyscoutUtils.getEvents(matchId, `${className}${fn}.historicalEventsByMatchId`, true);
      m.hasEvents = countEvents > 0;
      matches.push(m);
      if (matches.length >= 1000) {
        await wyscoutUtils.postDataProc('[dbo].[SetMatches]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn, true);
        await wyscoutUtils.setTeamsIds(teamsIdsMap, fn);
        await wyscoutUtils.setTeams(teamsMap, fn);
        matches = [];
        teamsIdsMap = {};
        teamsMap = {};
      }
    }
  }
  if (matches.length > 0) {
    await wyscoutUtils.postDataProc('[dbo].[SetMatches]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn, true);
    await wyscoutUtils.setTeamsIds(teamsIdsMap, fn);
    await wyscoutUtils.setTeams(teamsMap, fn);
    matches = [];
    teamsIdsMap = {};
    teamsMap = {};
  }

  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, true);
  // logger.info(`${fn} end`);
  return Promise.resolve();
};

const historicalScoresByMatchId = async () => {
  isStop = false;
  const fnName = 'historicalScoresByMatchId';
  const fn = `${className}${fnName}`;
  await wyscoutUtils.setLog('Info', fnName, null, 'Start', className, true);
  const matchesList = await wyscoutUtils.postDataProc('[dbo].[GetMatchesForHostorical]', [], [], fn, true);
  let matches = [];
  for (let i = 0; i < matchesList.length; i += 1) {
    if (isStop) {
      await wyscoutUtils.setLog('Info', fnName, null, 'End isStop=true', className, true);
      return Promise.resolve();
    }
    const matchId = matchesList[i].id;
    await wyscoutUtils.setLog('Info', fnName, null, `i:${i}-->matchId:${matchId}`, fn, true);

    await wyscoutUtils.sleep(100);

    const match = await wyscoutUtils.getTeamApi(matchId, true);
    if (match) {
      let m = {};
      m.matchId = _.get(match, 'wyId');
      m.duration = _.get(match, 'duration');
      m.winner = _.get(match, 'winner');

      let homeTeamId = -1;
      let awayTeamId = -1;
      let homeTeam;
      let awayTeam;
      const {teamsData} = match;
      if (teamsData) {
        Object.keys(teamsData).forEach((key) => {
          const oneTeam = teamsData[key] || {};
          if (oneTeam.side === 'home') {
            homeTeamId = oneTeam.teamId;
            homeTeam = oneTeam.team || {};
            homeTeam.scores = wyscoutUtils.getMatchScores(oneTeam);
          } else if (oneTeam.side === 'away') {
            awayTeamId = oneTeam.teamId;
            awayTeam = oneTeam.team || {};
            awayTeam.scores = wyscoutUtils.getMatchScores(oneTeam);
          }
        });
      }
      const {winner, duration} = m;
      const teamsByMatch = { homeTeamId, awayTeamId, homeTeam, awayTeam, winner, duration, match };
      m = wyscoutUtils.getMatchesScore(m, teamsByMatch.homeTeam, teamsByMatch.awayTeam);

      matches.push(m);
      if (matches.length >= 1000) {
        await wyscoutUtils.postDataProc('[dbo].[SetMatchesScore]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn, true);
        matches = [];
      }
    }
  }
  if (matches.length > 0) {
    await wyscoutUtils.postDataProc('[dbo].[SetMatches]', [], [{name: 'MatchesUD', columns: Config.tableDetails.matches, data: matches}], fn, true);
    matches = [];
  }

  await wyscoutUtils.setLog('Info', fnName, null, 'End', className, true);
  // logger.info(`${fn} end`);
  return Promise.resolve();
};

module.exports = {
  historicalEvents,
  historicalEventsFull,
  historicalEventsFullFromSql,
  historicalEventsByMatchId,
  historicalScoresByMatchId,
  stopHistorical
};
