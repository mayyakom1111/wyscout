# Script input
$env = $args[0]
$service = $args[1]
$isNoPromote = $args[2]

# Generate app.yaml file with service & env
$yaml_template = Get-Content -Path "yaml-template.txt" -Raw
$yaml_content = $yaml_template -f $service, $env
New-Item -Path "app.yaml" -ItemType "file" -Value $yaml_content -Force | Out-Null

# Get gcloud project
$project = "octo-research"
if ($env -eq "production") {
  $project = "octo-crawlers"
  $networkName = "octo-crawlers-vpc"
  $subnetworkName = "octo-crawlers-europe-west3"
}

# Deploy
if ($isNoPromote -eq "np") {
  # Deploy to new version without promote
  gcloud -q app deploy app.yaml --project $project --no-promote --no-stop-previous-version
} else {
  gcloud -q app deploy app.yaml --project $project
}