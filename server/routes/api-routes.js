'use strict';

const path = require('path');

const WyScoutController = require(path.resolve(__basedir, 'controllers/wyscout.controller'));

module.exports.route = (app) => {
  app.get('/api/v1/getWyScout', WyScoutController.getWyScout);
};
