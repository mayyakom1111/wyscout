'use strict';

const path = require('path');

global.__basedir = path.resolve(__dirname);
const express = require('express');
const chalk = require('chalk');
const logger = require('octo-common').services.OctoLogger;
/**
 * Load environment variables from .env files, where API keys and passwords are configured.
 */
require('dotenv-flow').config({
  cwd: path.resolve(__basedir, 'environments'),
  default_node_env: 'development'
});

const InitService = require(path.resolve(__basedir, 'services/init.service'));
/**
 * Create Express server.
 */
const app = express();
/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8084);
app.set('appName', process.env.APP_NAME || 'WyScout');

require(path.resolve(__basedir, 'routes/api-routes')).route(app);

/**
 * Start Express server.
 */
InitService.init().then(() => {
  app.listen(app.get('port'), () => {
    logger.info('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
    logger.info('  Press CTRL-C to stop\n');
  });
});

module.exports = app;
